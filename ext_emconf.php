<?php

/**
 * Extension Manager/Repository config file for ext "lesesaalsystematik".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'Lesesaalsystematik',
    'description' => 'Lss',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-9.5.99',
            'fluid_styled_content' => '9.5.0-9.5.99',
            'rte_ckeditor' => '9.5.0-9.5.99',
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'UbBraunschweig\\Lesesaalsystematik\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Henning Peters',
    'author_email' => 'henning.peters@tu-braunschweig.de',
    'author_company' => 'UB Braunschweig',
    'version' => '1.0.0',
];
